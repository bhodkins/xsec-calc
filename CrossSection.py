class CrossSection:
	""" 
	A class for getting cross-sections and filter efficiencies from the central PMG xsec database
	"""
	
	def __init__(
		self,
		pmg_dir = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/",
		pmg_file = "PMGxsecDB_mc16.txt",
        	separator="\t\t",
	):
		
		self.pmg_dir = pmg_dir
		self.pmg_file = pmg_file
		self.separator = separator
		self.crossSection = {}
		self.fEff = {}
		self.k = {}

	def setValues(self, dsid):
		found_dsid = 0
		try:
			with open(self.pmg_dir+self.pmg_file,'r') as f:
				for line in f:
					info = line.split(self.separator)
					if info[0]==dsid:
						found_dsid = 1
						break
		except OSError:
			print('WARNING: Cannot access cvmfs PMG file, using local copy which may be out of date')
			with open(self.pmg_file,'r') as f:
				for line in f:
					info = line.split(self.separator)
					if info[0]==dsid:
						found_dsid=1
						break
		if found_dsid==0:
			raise Exception("DSID not found in PMG file.")
		self.crossSection[dsid] = float(info[2])
		self.fEff[dsid] = float(info[3])
		self.k[dsid] = float(info[4])

	def xsec(self,dsid):
		dsid = str(dsid)
		if dsid not in self.crossSection:
			self.setValues(dsid)
		return self.crossSection[dsid]

	def filterEff(self,dsid):
		dsid = str(dsid)
		if dsid not in self.fEff:
			self.setValues(dsid)
		return self.fEff[dsid]

	def kFactor(self, dsid):
		dsid = str(dsid)
		if dsid not in self.k:
			self.setValues(dsid)
		return self.k[dsid]

	def xsecTimesEff(self, dsid):
		dsid = str(dsid)
		return self.xsec(dsid)*self.filterEff(dsid)

	def xsecTimesEffTimeskFac(self, dsid):
		dsid = str(dsid)
		return self.xsec(dsid)*self.filterEff(dsid)*self.kFactor(dsid)

				
